﻿using Microsoft.Win32;
using OxyPlot;
using SettingsSelection.Context;
using SettingsSelection.Helpers;
using SettingsSelection.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;

namespace SettingsSelection
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private readonly PlotViewModel _plotDataContext;
        private readonly IAnalysingHelper _analysingHelper;
        private List<DataPoint> _dataPoints;
        private List<DataPoint> _graphPoints;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = new PlotViewModel();
            _plotDataContext = (PlotViewModel)this.DataContext;
            _analysingHelper = new AnalysingHelper();
            _graphPoints = new List<DataPoint>();
        }

        #region UI controls' behaviour

        private void btn_LoadFile_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();

            openFileDialog.Filter = "Text files (*.txt)|*.txt";
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    _dataPoints = ReadSelectedFile(openFileDialog.FileName);
                    _plotDataContext.PlotFileReadPoints(_dataPoints);

                    var fileNameArray = openFileDialog.FileName.Split('\\');
                    txb_LoadFile.Text = $"Załadowano pomyślnie!\t{fileNameArray.Last()}";
                    txb_LoadFile.Foreground = new SolidColorBrush(Colors.DarkGreen);

                    ClearAllParameterTextBoxes();
                    _graphPoints.Clear();
                }
                catch
                {
                    txb_LoadFile.Text = "Błąd podczas ładowania pliku pomiarów!";
                    txb_LoadFile.Foreground = new SolidColorBrush(Colors.Red);
                }

            }
            else
            {
                txb_LoadFile.Text = "Nie wybrano pliku.";
                txb_LoadFile.Foreground = new SolidColorBrush(Colors.Orange);
            }
        }

        private void btn_ResetView_Click(object sender, RoutedEventArgs e)
        {
            _plotDataContext.ResetView();
        }

        private void txt_Scale_KeyDown(object sender, KeyEventArgs e)
        {
            var actualBorderColor = txt_Scale.BorderBrush;
            if (e.Key == Key.Return)
            {
               txt_Scale.MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
                try
                {
                    var scale = Convert.ToDouble(txt_Scale.Text);
                    _plotDataContext.Scale = scale;
                    _plotDataContext.OnScaleChange();
                    
                }
                catch(FormatException)
                {
                    txt_Scale.BorderBrush = new SolidColorBrush(Colors.Red);
                    var result = MessageBox.Show("Podana została wartość tekstowa jako skala linii pomocniczych. " +
                        "Proszę podać wartość liczbową!", 
                        "Błąd - zły format danych", 
                        MessageBoxButton.OK);

                    if(result == MessageBoxResult.OK)
                    {
                        txt_Scale.BorderBrush = actualBorderColor;
                    }
                }
            }
        }

        private void btn_Solve_Click(object sender, RoutedEventArgs e)
        {
            double t0K;
            double tauK;

            ProcessParameters processParameters = _analysingHelper.SetProcessParametersLeastSquaresMethod(_dataPoints);
            txt_K.Text = processParameters.K.ToString();
            txt_T0.Text = processParameters.T0.ToString();
            txt_Tau.Text = processParameters.Tau.ToString();

            for (int p = 0; p < _dataPoints.Last().X; p++)
            {
                var yt = _analysingHelper.ExponentFunctionInTime(p);
                if (yt < 0)
                {
                    yt = 0;
                }
                _graphPoints.Add(new DataPoint(p, yt));
            }

            _plotDataContext.DrawExponent(processParameters, _graphPoints);

            try
            {
                DataPoint t1Point;
                DataPoint t2Point;

                _analysingHelper.SetCriteriaTauAndT0(out t0K, out tauK, _graphPoints, out t1Point, out t2Point);
                txt_TauK.Text = tauK.ToString();
                txt_T0K.Text = t0K.ToString();

                _plotDataContext.Plot28_3And63_2(t1Point, t2Point);

                var qdrParams = _analysingHelper.SettingsForQDRMethod(t0K, tauK);
                var integralParams = _analysingHelper.SettingsForOvershoot20Percent(t0K, tauK);
                var overshootParams = _analysingHelper.SettingsForOvershoot0Percent(t0K, tauK);

                txt_KR_0.Text = overshootParams.KR.ToString();
                txt_Td_0.Text = overshootParams.Td.ToString();
                txt_Ti_0.Text = overshootParams.Ti.ToString();

                txt_KR_I2.Text = integralParams.KR.ToString();
                txt_Td_I2.Text = integralParams.Td.ToString();
                txt_Ti_I2.Text = integralParams.Ti.ToString();

                //txt_KR_QDR.Text = qdrParams.KR.ToString();
                //txt_Td_QDR.Text = qdrParams.Td.ToString();
                //txt_Ti_QDR.Text = qdrParams.Ti.ToString();

                txt_KR_QDR.IsEnabled = false;
                txt_Td_QDR.IsEnabled = false;
                txt_Ti_QDR.IsEnabled = false;


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Błąd", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        #endregion
        #region Helping private methods
        private List<DataPoint> ReadSelectedFile(string fileName)
        {
            string line;
            List<DataPoint> returnPoints = new List<DataPoint>();
            using (var streamReader = new StreamReader(fileName))
            {
                while ((line = streamReader.ReadLine()) != null)
                {
                    line.Replace(" ", "\t").TrimEnd();
                    var pointsArray = line.Split("\t");
                    var tempDouble = Convert.ToDouble(pointsArray[0]);
                    var x = Convert.ToInt32(tempDouble);
                    var y = Convert.ToDouble(pointsArray[4]);
                    returnPoints.Add(new DataPoint(x, y));
                }
                streamReader.Close();
            }
            if (returnPoints.Count == 0)
            {
                throw new InvalidDataException();
            }
            return returnPoints;
        }

        private void ClearAllParameterTextBoxes()
        {
            txt_K.Text = "";
            txt_KR_0.Text = "";
            txt_KR_I2.Text = "";
            txt_KR_QDR.Text = "";

            txt_T0.Text = "";
            txt_T0K.Text = "";

            txt_Tau.Text = "";
            txt_TauK.Text = "";

            txt_Td_0.Text = "";
            txt_Td_I2.Text = "";
            txt_Td_QDR.Text = "";

            txt_Ti_0.Text = "";
            txt_Ti_I2.Text = "";
            txt_Ti_QDR.Text = "";
        }

        #endregion

    }
}
