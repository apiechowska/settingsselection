﻿using OxyPlot;
using OxyPlot.Annotations;
using OxyPlot.Axes;
using OxyPlot.Series;
using SettingsSelection.Helpers;
using SettingsSelection.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SettingsSelection.Context
{

    public class PlotViewModel
    {
        public PlotModel Model { get; private set; }
        public double Scale { get; set; } = 5.0;

        private IExtraLinesHelper _extraLinesHelper { get; set; }
        private double _maxX { get; set; } = 100;
        private double _maxY { get; set; } = 100;
        public PlotViewModel()
        {
            Model = new PlotModel { Title = "Zależność przepływu masowego od czasu ṁ(t)" };
            _extraLinesHelper = new ExtraLinesHelper(Model);

            Model.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Bottom,
                AbsoluteMinimum = -1,
                AbsoluteMaximum = 101,
                Title = "Czas t[s]"
            });
            Model.Axes.Add(new LinearAxis
            {
                Position = AxisPosition.Left,
                AbsoluteMinimum = -1,
                AbsoluteMaximum = 101,
                Title = "Przepływ masowy ṁ[kg/h]"
            });

            OnScaleChange();
        }

        public void PlotFileReadPoints(List<DataPoint> dataPoints)
        {
            _maxX = double.MinValue;
            _maxY = double.MinValue;

            var series = new LineSeries();
            series.Color = OxyColors.Transparent;
            series.MarkerFill = OxyColors.SteelBlue;
            series.MarkerType = MarkerType.Circle;

            Model.Series.Clear();

            foreach (var point in dataPoints)
            {
                series.Points.Add(point);
                _maxX = point.X > _maxX ? point.X : _maxX;
                _maxY = point.Y > _maxY ? point.Y : _maxY;
            }
            
            SetAbsoluteMaximumAxis();
            _extraLinesHelper.SetHelpLines(_maxX, _maxY, Scale);
            
            Model.Series.Add(series);
            Model.InvalidatePlot(true);
        }

        public void DrawExponent(ProcessParameters processParameters, List<DataPoint> graphPoints)
        {
            var series = new LineSeries();
            series.Color = OxyColors.Green;
            series.MarkerFill = OxyColors.Transparent;
            series.MarkerType = MarkerType.None;

            series.Points.AddRange(graphPoints);
            series.Points.Add(new DataPoint(processParameters.T0, 0));
            series.Points.Sort(delegate (DataPoint a, DataPoint b) { return a.X.CompareTo(b.X); });

            Model.Series.Add(series);
            Model.InvalidatePlot(true);
        }

        public void Plot28_3And63_2(DataPoint t1, DataPoint t2)
        {
            var series = new LineSeries();
            series.Color = OxyColors.Transparent;
            series.MarkerFill = OxyColors.Red;
            series.MarkerType = MarkerType.Square;

            series.Points.Add(t2);
            series.Points.Add(t1);

            Model.Series.Add(series);
            Model.InvalidatePlot(true);
        }

        public void OnScaleChange()
        {
            _extraLinesHelper.SetHelpLines(_maxX, _maxY, Scale);
            Model.InvalidatePlot(true);
        }
        private void SetAbsoluteMaximumAxis()
        {
            var xAxis = Model.Axes.First(a => a.Position == AxisPosition.Bottom);
            var yAxis = Model.Axes.First(a => a.Position == AxisPosition.Left);

            xAxis.AbsoluteMaximum = _maxX + 1;
            yAxis.AbsoluteMaximum = _maxY + 100;
        }
        public void ResetView()
        {
            Model.ResetAllAxes();
            Model.InvalidatePlot(true);
        }
    }
}
