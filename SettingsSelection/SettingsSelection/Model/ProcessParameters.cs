﻿namespace SettingsSelection.Model
{
    public class ProcessParameters
    {
        public double K { get; set; }
        public double T0 { get; set; }
        public double Tau { get; set; }
    }
}
