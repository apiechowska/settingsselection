﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SettingsSelection.Model
{
    public class ControllerSettings
    {
        public double KR { get; set; }
        public double Ti { get; set; }
        public double Td { get; set; }
    }
}
