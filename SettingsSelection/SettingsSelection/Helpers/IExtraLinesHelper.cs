﻿using OxyPlot;

namespace SettingsSelection.Helpers
{
    public interface IExtraLinesHelper
    {
        PlotModel Model { get; set; }

        void SetHelpLines(double maxX, double maxY, double scale);

    }
}
