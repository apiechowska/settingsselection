﻿using OxyPlot;
using OxyPlot.Annotations;

namespace SettingsSelection.Helpers
{
    public class ExtraLinesHelper : IExtraLinesHelper
    {
        public PlotModel Model { get; set; }

        public ExtraLinesHelper(PlotModel model)
        {
            Model = model;
        }

        public void SetHelpLines(double maxX, double maxY, double scale)
        {
            Model.Annotations.Clear();

            var xAxisDivisionReminder = maxX % scale;
            var yAxisDivisionReminder = maxY % scale;

            var lastHelpLineX = maxX - xAxisDivisionReminder;
            var lastHelpLineY = maxY - yAxisDivisionReminder;

            AddHelpLines(lastHelpLineX, scale, LineAnnotationType.Vertical);
            AddHelpLines(lastHelpLineY, scale * 100, LineAnnotationType.Horizontal);
        }

        private void AddHelpLines(double maxValue, double scale, LineAnnotationType lineType)
        {
            if (maxValue >= scale)
            {
                for (double i = scale; i <= maxValue; i += scale)
                {
                    var line = new LineAnnotation()
                    {
                        StrokeThickness = 0.5,
                        Color = OxyColors.DimGray,
                        Type = lineType,
                        TextColor = OxyColors.White,
                        X = lineType == LineAnnotationType.Vertical ? i : 0,
                        Y = lineType == LineAnnotationType.Horizontal ? i : 0
                    };
                    Model.Annotations.Add(line);
                }
            }
        }
    }
}
