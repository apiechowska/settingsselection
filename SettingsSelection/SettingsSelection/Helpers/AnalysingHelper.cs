﻿using OxyPlot;
using SettingsSelection.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SettingsSelection.Helpers
{
    public class AnalysingHelper : IAnalysingHelper
    {
        private ProcessParameters _processParameters { get; set; }

        public AnalysingHelper()
        {
            _processParameters = new ProcessParameters();
        }

        public ProcessParameters SetProcessParametersLeastSquaresMethod(List<DataPoint> points)
        {
            double minSum = double.MaxValue;

            MinimizeT0Range(points, out var minT0, out var maxT0);
            MinimizeKRange(points, out var minK, out var maxK);

            var options = new ParallelOptions()
            {
                MaxDegreeOfParallelism = 4
            };

            Parallel.For(minK, maxK, options, K =>
            { 
                for (double T0 = minT0; T0 <= maxT0; T0 += 0.2)
                {
                    for (double tau = 0.1; tau <= 30; tau += 0.1)
                    {
                        var tempSum = 0.0;
                        var overMinimum = false;

                        foreach (var point in points)
                        {
                            var yt = K * (1 - Math.Exp(-((point.X - T0) / tau)));
                            tempSum += Math.Pow((yt - point.Y), 2);
                            if (tempSum > minSum)
                            {
                                overMinimum = true;
                                break;
                            }
                        }

                        if (tempSum < minSum && !overMinimum)
                        {
                            minSum = tempSum;
                            _processParameters.K = K;
                            _processParameters.T0 = T0;
                            _processParameters.Tau = tau;
                        }

                    }
                }
            });

            _processParameters.T0 = Math.Round(_processParameters.T0, 2);
            _processParameters.Tau = Math.Round(_processParameters.Tau, 2);
            return _processParameters;
        }

        private void MinimizeT0Range(List<DataPoint> points, out double minT0, out double maxT0)
        {
            var maxPointValue = points.Max(p => p.Y);
            var yLimitT0 = maxPointValue * (3.2 / 100);

            var limitT0 = 0.0;
            for (int i = 0; i < points.Count; i++)
            {
                if (points[i].Y >= yLimitT0)
                {
                    limitT0 = points[Math.Max(0, i - 1)].X;
                    break;
                }
            }

            var limitT0Up = limitT0 + 10;
            var limitT0Down = limitT0 - 0.2;

            minT0 = limitT0Down < 0 ? 0 : limitT0Down - limitT0Down % 0.2;
            maxT0 = limitT0Up > 20 ? 20 : limitT0Up - limitT0Up % 0.2 + 0.2;

        }
        
        private void MinimizeKRange(List<DataPoint> points, out int minK, out int maxK)
        {
            var maxPointValue = points.Max(p => p.Y);

            var limitKUp = maxPointValue + 500;
            var limitKDown = maxPointValue - 500;

            minK = limitKDown < 2000 ? 2000 : (int)(limitKDown - limitKDown % 1);
            maxK = limitKUp > 4600 ? 4600 : (int)(limitKUp - limitKUp % 1 + 1);
        }


        public double ExponentFunctionInTime(double t)
        {
            return _processParameters.K * (1 - Math.Exp(-((t - _processParameters.T0) / _processParameters.Tau)));
        }

        public void SetCriteriaTauAndT0(out double t0K, out double tauK, List<DataPoint> graphPoints,
            out DataPoint t1Point, out DataPoint t2Point)
        {
            t0K = 0.0;
            tauK = 0.0;

            t1Point = new DataPoint();
            t2Point = new DataPoint();

            FindT1AndT2Points(out t1Point, out t2Point, graphPoints);

            tauK = 1.5 * (t1Point.X - t2Point.X);
            t0K = t1Point.X - tauK;

            tauK = Math.Round(tauK, 5);
            t0K = Math.Round(t0K, 5);

            if (0.2 > t0K / tauK || t0K / tauK > 0.7)
            {
                throw new Exception("Nie został spełniony warunek parametru niekontrolowalności w obliczeniach");
            }
        }

        private void FindT1AndT2Points(out DataPoint t1, out DataPoint t2, List<DataPoint> graphPoints)
        {
            t1 = new DataPoint();
            t2 = new DataPoint();

            DataPoint t1min;
            DataPoint t2min;
            DataPoint t1max;
            DataPoint t2max;

            var t1Found = false;
            var t2Found = false;

            var yt1 = _processParameters.K * (63.2 / 100);
            var yt2 = _processParameters.K * (28.3 / 100);

            for (int i = 0; i < graphPoints.Count; i++)
            {
                if (graphPoints[i].Y >= yt1 && !t1Found)
                {
                    t1min = graphPoints[Math.Max(0, i - 1)];
                    t1max = graphPoints[i];
                    t1 = BisectionAlgorythm(yt1, t1min, t1max);
                    t1Found = true;
                }

                if (graphPoints[i].Y >= yt2 && !t2Found)
                {
                    t2min = graphPoints[Math.Max(0, i - 1)];
                    t2max = graphPoints[i];
                    t2 = BisectionAlgorythm(yt2, t2min, t2max);
                    t2Found = true;
                }

                if (t1Found && t2Found)
                {
                    break;
                }
            }
        }

        private DataPoint BisectionAlgorythm(double searchedY, DataPoint minPoint, DataPoint maxPoint)
        {
            var i = 0;

            var halfX = 0.0;
            var halfY = 0.0;
            var precisionX = 1e-5;
            var precisionY = 1e-5;

            if (minPoint.Y == searchedY)
            {
                return minPoint;
            }

            if (maxPoint.Y == searchedY)
            {
                return maxPoint;
            }

            var minX = minPoint.X;

            var maxX = maxPoint.X;

            while (true)
            {
                i++;

                halfX = (minX + maxX) / 2;
                if (Math.Abs(minX - halfX) < precisionX) { break; }

                halfY = ExponentFunctionInTime(halfX);

                if (Math.Abs(searchedY - halfY) < precisionY) { break; }

                if (halfY - searchedY < 0)
                {
                    minX = halfX;
                }
                else
                {
                    maxX = halfX;
                }
            }

            return new DataPoint(halfX, halfY);
        }

        public ControllerSettings SettingsForQDRMethod(double t0K, double tauK)
        {
            var controllerSettings = new ControllerSettings();

            controllerSettings.KR = Math.Round((1.2 * tauK) / (_processParameters.K * t0K), 5);
            controllerSettings.Ti = Math.Round(2 * t0K, 5);
            controllerSettings.Td = Math.Round(0.5 * t0K, 5);

            return controllerSettings;
        }

        public ControllerSettings SettingsForOvershoot20Percent(double t0K, double tauK)
        {
            var controllerSettings = new ControllerSettings();

            controllerSettings.KR = Math.Round((1.1 * tauK) / (t0K), 5);
            controllerSettings.Ti = Math.Round(2.0 * t0K, 5);
            controllerSettings.Td = Math.Round(0.53 * t0K, 5);

            return controllerSettings;
        }

        public ControllerSettings SettingsForOvershoot0Percent(double t0K, double tauK)
        {
            var controllerSettings = new ControllerSettings();

            controllerSettings.KR = Math.Round((0.65 * tauK) / ( t0K), 5);
            controllerSettings.Ti = Math.Round(5.0 * t0K, 5);
            controllerSettings.Td = Math.Round(0.23 * t0K, 5);

            return controllerSettings;
        }
    }
}
