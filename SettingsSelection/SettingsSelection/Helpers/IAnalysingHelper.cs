﻿using OxyPlot;
using SettingsSelection.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace SettingsSelection.Helpers
{
    public interface IAnalysingHelper
    {
        ProcessParameters SetProcessParametersLeastSquaresMethod(List<DataPoint> points);
        void SetCriteriaTauAndT0(out double t0K, out double tauK, List<DataPoint> graphPoints,
            out DataPoint t1Point, out DataPoint t2Point);
        double ExponentFunctionInTime(double t);
        ControllerSettings SettingsForQDRMethod(double t0K, double tauK);
        ControllerSettings SettingsForOvershoot20Percent(double t0K, double tauK);
        ControllerSettings SettingsForOvershoot0Percent(double t0K, double tauK);
    }
}
